package br.com.ralves.braveat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BraveatApplication {

	public static void main(String[] args) {
		SpringApplication.run(BraveatApplication.class, args);
	}

}
